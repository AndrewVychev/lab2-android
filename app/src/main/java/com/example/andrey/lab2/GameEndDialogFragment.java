package com.example.andrey.lab2;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

/**
 * Created by andrey on 11/12/16.
 */

public class GameEndDialogFragment extends DialogFragment {

    private static final String ARG_TIME = "time";

    private Listener mListener;

    public static GameEndDialogFragment newInstance(long time) {
        Bundle bundle = new Bundle(1);
        bundle.putLong(ARG_TIME, time);
        GameEndDialogFragment gameEndDialogFragment = new GameEndDialogFragment();
        gameEndDialogFragment.setArguments(bundle);
        return gameEndDialogFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (Listener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement Listener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        float time = getArguments().getLong(ARG_TIME) / 1000F;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String message = String.format(getString(R.string.game_end_message).toString(), time);
        builder.setMessage(message)
                .setPositiveButton(R.string.play_more, (dialog, id) -> mListener.playMore())
                .setNegativeButton(R.string.exit, (dialogInterface, i) -> mListener.finishGame());
        return builder.create();
    }

    public interface Listener {

        void playMore();

        void finishGame();

    }
}
