package com.example.andrey.lab2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appyvet.rangebar.RangeBar;

public class MainActivity extends AppCompatActivity
        implements RangeBar.OnRangeBarChangeListener, GameEndDialogFragment.Listener {

    private EditText mInputValue;
    private Button mEnterValue;
    private Button mStartGame;
    private RangeBar mRangeBar;
    private TextView mTitle;
    private LinearLayout mShowGame;
    private long mStartTime;
    private long mFinishTime;
    private int guess;
    private boolean gameFinished = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mInputValue = (EditText) findViewById(R.id.inputValue);
        mEnterValue = (Button) findViewById(R.id.enterValue);
        mRangeBar = (RangeBar) findViewById(R.id.rangebar);
        mRangeBar.setOnRangeBarChangeListener(this);
        mShowGame = (LinearLayout) findViewById(R.id.showGame);
        mStartGame = (Button) findViewById(R.id.startGame);
        mTitle = (TextView) findViewById(R.id.title);
        mTitle.setText(String.format(getString(R.string.try_to_guess).toString(),
                mRangeBar.getLeftIndex() + 1, mRangeBar.getRightIndex() + 1));
    }

    @Override
    public void onRangeChangeListener(RangeBar rangeBar,
                                      int leftPinIndex,
                                      int rightPinIndex,
                                      String leftPinValue,
                                      String rightPinValue) {
        int left = Integer.valueOf(leftPinValue);
        int right = Integer.valueOf(rightPinValue);
        mTitle.setText(String.format(getText(R.string.try_to_guess).toString(), left, right));
    }

    private void toast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void startGame(View view) {
        int left = Integer.valueOf(mRangeBar.getLeftPinValue());
        int right = Integer.valueOf(mRangeBar.getRightPinValue());
        guess = left + (int) (Math.random() * ((right - left) + 1));
        mShowGame.setVisibility(View.VISIBLE);
        mStartGame.setVisibility(View.GONE);
        mRangeBar.setEnabled(false);
        mStartTime = System.currentTimeMillis();
    }

    public void onCheckValue(View v) {
        try {
            if (!gameFinished) {
                int inp = Integer.parseInt(mInputValue.getText().toString());
                int left = Integer.parseInt(mRangeBar.getLeftPinValue());
                int right = Integer.parseInt(mRangeBar.getRightPinValue());
                if (inp > right || inp < left)
                    throw new NumberFormatException();
                if (inp > guess)
                    toast(getResources().getString(R.string.ahead));
                if (inp < guess)
                    toast(getResources().getString(R.string.behind));
                if (inp == guess) {
                    toast(getResources().getString(R.string.hit));
                    mShowGame.setVisibility(View.GONE);
                    gameFinished = true;
                    mFinishTime = System.currentTimeMillis();
                    GameEndDialogFragment.newInstance(mFinishTime - mStartTime)
                            .show(getSupportFragmentManager(), "Game end dialog");
                }
            } else {
                guess = (int) (Math.random() * 100);
                mEnterValue.setText(getResources().getString(R.string.input_value));
                gameFinished = false;
            }
            mInputValue.setText("");
        } catch (Exception e) {
            toast(getResources().getString(R.string.error));
        }
    }

    @Override
    public void playMore() {
        mStartGame.setVisibility(View.VISIBLE);
        mRangeBar.setEnabled(true);
    }

    @Override
    public void finishGame() {
        finish();
    }
}
